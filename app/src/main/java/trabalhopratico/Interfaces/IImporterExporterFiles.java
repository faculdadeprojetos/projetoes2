package trabalhopratico.Interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import com.opencsv.exceptions.CsvException;

import trabalhopratico.Classes.Client;
import trabalhopratico.Classes.Reservation;
import trabalhopratico.Classes.StatisticsGeral;

// Interface for all importer and exporter methods to be implemented
public interface IImporterExporterFiles {

    /**
     * Importer Method that reads from a CSVFile
     * 
     * @param fileName     File Name to import data from
     * @param clients      Array of clients to store the data
     * @param reservations Array of reservations to store the data
     */
    public String importer(String fileName, ArrayList<Client> clients, ArrayList<Reservation> reservations)
            throws IOException, CsvException, ParseException;

    public String exporter(String fileName, ArrayList<Client> clients, StatisticsGeral estatisticas) throws IOException;
}
