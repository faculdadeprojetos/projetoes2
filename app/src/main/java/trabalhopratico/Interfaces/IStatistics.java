package trabalhopratico.Interfaces;

import java.text.ParseException;
import java.util.ArrayList;

import trabalhopratico.Classes.Client;
import trabalhopratico.Classes.Reservation;
import trabalhopratico.Enumerations.DistributionChannel;

public interface IStatistics {
  // #region PREFERED SEASONLITY

  /**
   * Qual a altura do ano preferida no geral (p.ex. Outono, Inverno, Primavera ou
   * Verão)
   * 
   * @param clients é {@link Client#getReservation() purchasesDate}
   * @return altura do ano preferida no geral
   * @throws ParseException
   */
  public String getPreferedSeasonalityGeneral(ArrayList<Reservation> reservations);

  /**
   * Qual a altura do ano preferida de cada cliente (p.ex. Outono, Inverno,
   * Primavera ou Verão)
   * 
   * @param clients is {@link Client#getReservation() purchasesDate}
   * @return altura do ano preferida de cada cliente
   */
  public String getPreferedSeasonalityByClient(Client client);

  // #endregion

  // #region DISTRIBUTION CHANNEL

  /**
   * Qual o distribution channel mais utilizado no geral para fazer a reserva
   * 
   * @param clients é {@link Client#getReservation() distributionChannel}
   * @return distribution channel mais utilizado no geral para fazer a reserva
   */
  public DistributionChannel getDistributionChannelGeneral(ArrayList<Reservation> reservations);

  // #endregion

  // #region VALUE RESERVATION

  /**
   * Para o indicador de score monetização, realizar-se-á a média total do valor
   * de compras realizadas. (lodgingRevenue + otherRevenue / nº total de reservas)
   * 
   * @param clients é {@link Client#getReservation() lodgingRevenue, otherRevenue,
   *                bookingCheckIn}
   * @return média total do valor de compras realizadas
   */
  public double getAverageValueReservation(ArrayList<Reservation> reservations);

  /**
   * Para o indicador de score monetização, verificar qual o menor valor de
   * compras realizadas. (lodgingRevenue + otherRevenue)
   * 
   * @param clients é {@link Client#getReservation() lodgingRevenue, otherRevenue}
   * @return menor valor de compras realizadas
   */
  public double getMinValueReservation(ArrayList<Reservation> reservations);

  /**
   * Para o indicador de score monetização, verificar qual o maior valor de
   * compras realizadas. (lodgingRevenue + otherRevenue)
   * 
   * @param clients é {@link Client#getReservation() lodgingRevenue, otherRevenue}
   * @return maior valor de compras realizadas
   */
  public double getMaxValueReservation(ArrayList<Reservation> reservations);

  // #endregion

  // #region PAYMENT METHOD

  /**
   * Qual o método de pagamento mais utilizado no geral para o pagamento das
   * reservas
   * 
   * @param clients é {@link Client#getReservation() paymentMethod}
   * @return método de pagamento mais utilizado no geral
   */
  public String getPaymentMethodGeneral(ArrayList<Reservation> reservations);

  /**
   * Qual o método de pagamento preferido por cada cliente para o pagamento da
   * reserva
   * 
   * @param clients é {@link Client#getReservation() paymentMethod}
   * @return método de pagamento preferido por cada cliente
   */
  public String getPaymentMethodByClient(Client client);

  // #endregion

  // #region INTERVAL TIME BETWEEN PURCHASES

  /**
   * Qual a média de intervalo de tempo entre compras em geral, ou seja, fazer a
   * média de intervalo de compras para cada cliente, somar e dividir pelo número
   * de clientes
   * 
   * @param clients é {@link Client#getReservation() purchasesDate}
   * @return média de intervalo de tempo entre compras em geral
   */
  public double getAverageIntervalTimeBetweenPurchasesGeneral(ArrayList<Reservation> reservations);

  /**
   * Qual a média de intervalo de tempo entre compras em cada cliente, ou seja, se
   * um cliente tiver feito 4 compras, ver o intervalo de tempo entre a 1 e 2
   * compra, entre a 2 e a 3 compra e entre a 3 e 4 compra, somar e dividir por 3
   * 
   * @param clients é {@link Client#getReservation() purchasesDate}
   * @return média de intervalo de tempo entre compras em cada cliente
   */
  public double getAverageIntervalTimeBetweenPurchasesByClient(Client client);

  /**
   * Qual o intervalo de tempo máximo entre compras para cada cliente, isto é
   * verifica-se o intervalo de tempo entre a 1 e 2 compra, entre a 2 e a 3 compra
   * e entre a 3 e 4 compra, e depois compara-se qual dos intervalos de tempos é
   * maior
   * 
   * @param clients é {@link Client#getReservation() purchasesDate}
   * @return intervalo de tempo máximo entre compras para cada cliente
   */
  public long getMaxIntervalTimeBetweenPurchasesClient(Client clients);

  /**
   * Qual o intervalo de tempo máximo entre compras em geral, ou seja, na
   * totalidade de clientes, isto é, verifica-se o máximo de cada cliente e depois
   * o máximo entre todos os clientes
   * 
   * @param clients é {@link Client#getReservation() purchasesDate}
   * @return intervalo de tempo máximo entre compras em geral
   */
  public long getMaxIntervalTimeBetweenPurchasesGeneral(ArrayList<Client> clients);

  /**
   * Qual o intervalo de tempo mínimo entre compras para cada cliente, isto é
   * verifica-se o intervalo de tempo entre a 1 e 2 compra, entre a 2 e a 3 compra
   * e entre a 3 e 4 compra, e depois compara-se qual dos intervalos de tempos é
   * menor
   * 
   * @param clients é {@link Client#getReservation() purchasesDate}
   * @return intervalo de tempo mínimo entre compras para cada cliente
   */
  public long getMinIntervalTimeBetweenPurchasesClient(Client clients);

  /**
   * Qual o intervalo de tempo minímo entre compras em geral, ou seja, na
   * totalidade de clientes, isto é, verifica-se o mínimo de cada cliente e depois
   * o mínimo entre todos os clientes
   * 
   * @param clients is {@link Client#getReservation() purchasesDate}
   * @return intervalo de tempo minímo entre compras em geral
   */
  public long getMinIntervalTimeBetweenPurchasesGeneral(ArrayList<Client> clients);

  // #endregion

  // #region ALL SCORE
  /**
   * Get customer who has the highest monetary amount spent on hotel reservations
   * 
   * @param clients {@Link ClientScoreManagement#clientes clientes}
   * @return a Client
   */
  public Client getClientMaxValueMonetization(ArrayList<Client> clients);

  /**
   * Get the customer with the lowest amount of money spent on hotel reservations
   * 
   * @param clients {@Link ClientScoreManagement#clientes clientes}
   * @return a Client
   */
  public Client getClientMinValueMonetization(ArrayList<Client> clients);

  /**
   * Get the customer with a greater regularity in hotel reservations
   * 
   * @param clients {@Link ClientScoreManagement#clientes clientes}
   * @returna a Client
   */
  public Client getClientMaxRegularity(ArrayList<Client> clients);

  /**
   * Get the customer with less regularity in hotel reservations
   * 
   * @param clients {@Link ClientScoreManagement#clientes clientes}
   * @return a Client
   */
  public Client getClientMinRegularity(ArrayList<Client> clients);

  /**
   * Obtain the customer with the maximum amount of total reservations made at the
   * hotel
   * 
   * @param clients {@Link ClientScoreManagement#clientes clientes}
   * @return a client
   */
  public Client getClientMaxTotalBookings(ArrayList<Client> clients);

  /**
   * Get the customer with the minimum amount of total reservations made at the
   * hotel
   * 
   * @param clients {@Link ClientScoreManagement#clientes clientes}
   * @return a client
   */
  public Client getClientMinTotalBookings(ArrayList<Client> clients);

  /**
   * Get the customer with the best average scores
   * 
   * @param clients {@Link ClientScoreManagement#clientes clientes}
   * @return a client
   */
  // public Client getClientBetterAverageScore (ArrayList<Client> clients);

  /**
   * Get the customer with the worst average score
   * 
   * @param clients {@Link ClientScoreManagement#clientes clientes}
   * @return a client
   */
  // public Client getClientWorstAverageScore (ArrayList<Client> clients);

  // #endregion
}
