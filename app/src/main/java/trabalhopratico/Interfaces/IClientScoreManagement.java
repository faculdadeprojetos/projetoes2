package trabalhopratico.Interfaces;

public interface IClientScoreManagement {
    /*
     * Adds the regularity score to all clients based on the frequency of the purchases they've made
     */
    public String addRegularidadeScore();

    /*
     * Adds the total number of purchases score to all clients based on the amount of purchases they've made
     */
    public String addTotalComprasScore();

    /*
     * Adds the monetization score to all clients based on the money they've spent
     */
    public String addMonetizacaoScore();
}
