
package trabalhopratico.Classes;


public class ReservationPreferences {
    /*
     * Entry of the preference in the dataset
     */
    private int ID;

    /*
     * High floor preference in a reservation
     */
    private int SRHighFloor;

    /*
     * Low floor preference in a reservation
     */
    private int SRLowFloor;

    /*
     * Medium floor preference in a reservation
     */
    private int SRMediumFloor;

    /*
     * Accesible room preference in a reservation
     */
    private int SRAccessibleRoom;

    /*
     * Bathtub room preference in a reservation
     */
    private int SRBathtub;

    /*
     * Shower room preference in a reservation
     */
    private int SRShower;

    /*
     * Crib room preference in a reservation
     */
    private int SRCrib;

    /*
     * KingSizedBed room preference in a reservation
     */
    private int SRKingSizeBed;

    /*
     * TwinBed room preference in a reservation
     */
    private int SRTwinBed;

    /*
     * NearElevator room preference in a reservation
     */
    private int SRNearElevator;

    /*
     * AwayFromElevator room preference in a reservation
     */
    private int SRAwayFromElevator;

    /*
     * Quiet room preference in a reservation
     */
    private int SRQuietRoom;

    /*
     * AlcoholInMiniBar room preference in a reservation
     */
    private int SRNoAlcoholInMiniBar;

     /**
      * Constructor for an instance of reservation preferences
      * @param idPreferences
      * @param SRHighFloor
      * @param SRLowFloor
      * @param SRMediumFloor
      * @param SRAccessibleRoom
      * @param SRBathtub
      * @param SRShower
      * @param SRCrib
      * @param SRKingSizeBed
      * @param SRTwinBed
      * @param SRNearElevator
      * @param SRAwayFromElevator
      * @param SRQuietRoom
      * @param SRNoAlcoholInMiniBar
      */
    public ReservationPreferences(int ID, int SRHighFloor, int SRLowFloor, int SRMediumFloor, int SRAccessibleRoom, int SRBathtub, int SRShower, int SRCrib, int SRKingSizeBed, int SRTwinBed, int SRNearElevator, int SRAwayFromElevator, int SRQuietRoom, int SRNoAlcoholInMiniBar) 
    {
        this.ID = ID;
        this.SRHighFloor = SRHighFloor;
        this.SRLowFloor = SRLowFloor;
        this.SRMediumFloor = SRMediumFloor;
        this.SRAccessibleRoom = SRAccessibleRoom;
        this.SRBathtub = SRBathtub;
        this.SRShower = SRShower;
        this.SRCrib = SRCrib;
        this.SRKingSizeBed = SRKingSizeBed;
        this.SRTwinBed = SRTwinBed;
        this.SRNearElevator = SRNearElevator;
        this.SRAwayFromElevator = SRAwayFromElevator;
        this.SRQuietRoom = SRQuietRoom;
        this.SRNoAlcoholInMiniBar = SRNoAlcoholInMiniBar;
    }

    /**
     * Return the preference entry in the dataset
     * 
     * @return
     */
    public int getId() {
        return this.ID;
    }

    /**
     * Returns the HighFloor preference associated to this reservation
     * 
     * @return HighFloor preference associated to this reservation
     */
    public int getSRHighFloor() {
        return this.SRHighFloor;
    }

    /**
     * Return the LowFloor preference associated to this reservation
     * 
     * @return LowFloor preference associated to this reservation
     */
    public int getSRLowFloor() {
        return this.SRLowFloor;
    }

    /**
     * Returns the MediumFloor preference associated to this reservation
     * 
     * @return MediumFloor preference associated to this reservation
     */
    public int getSRMediumFloor() {
        return this.SRMediumFloor;
    }

    /**
     * Returns the Accessible room preference associated to this reservation
     * 
     * @return Accessible room preference associated to this reservation
     */
    public int getSRAccessibleRoom() {
        return this.SRAccessibleRoom;
    }

    /**
     * Returns the Bathtub room preference associated to this reservation
     * 
     * @return Bathtub room preference associated to this reservation
     */
    public int getSRBathtub() {
        return this.SRBathtub;
    }

    /**
     * Returns the Shower room preference associated to this reservation
     * 
     * @return Shower room preference associated to this reservation
     */
    public int getSRShower() {
        return this.SRShower;
    }

    /**
     * Returns the Crib room prerefence associated to this reservation
     * 
     * @return Crib room preference associated to this reservation
     */
    public int getSRCrib() {
        return this.SRCrib;
    }

    /**
     * Returns the KingSizeBed room preference associated to this reservation
     * 
     * @return KingSizeBed room preference associated to this reservation
     */
    public int getSRKingSizeBed() {
        return this.SRKingSizeBed;
    }

    /**
     * Returns the TwinBed room preference associated to this reservation
     * 
     * @return TwinBed room preference associated to this reservation
     */
    public int getSRTwinBed() {
        return this.SRTwinBed;
    }

    /**
     * Return the NearElevator room preference associated to this reservation
     * 
     * @return NearElevator room preference associated to this reservation
     */
    public int getSRNearElevator() {
        return this.SRNearElevator;
    }

    /**
     * Return the AwayFromElevator room preference associated to this reservation
     * 
     * @return AwayFromElevator room preference associated to this reservation
     */
    public int getSRAwayFromElevator() {
        return this.SRAwayFromElevator;
    }

    /**
     * Return the QuietRoom room preference associated to this reservation
     * 
     * @return QuietRoom room preference associated to this reservation
     */
    public int getSRQuietRoom() {
        return this.SRQuietRoom;
    }

    /**
     * Return the NoAlcoholInMiniBar room preference associated to this reservation
     * 
     * @return NoAlcoholInMiniBar room preference associated to this reservation
     */
    public int getSRNoAlcoholInMiniBar() {
        return this.SRNoAlcoholInMiniBar;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#ID ID}
     * 
     * @param ID the {@link ReservationPreferences#ID ID}
     */
    public void setId(int ID) {
        this.ID = ID;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRHighFloor SRHighFloor}
     * 
     * @param SRHighFloor the {@link ReservationPreferences#SRHighFloor SRHighFloor}
     */
    public void setSRHighFloor(int SRHighFloor) {
        this.SRHighFloor = SRHighFloor;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRLowFloor SRLowFloor}
     * 
     * @param SRLowFloor the {@link ReservationPreferences#SRLowFloor SRLowFloor}
     */
    public void setSRLowFloor(int SRLowFloor) {
        this.SRLowFloor = SRLowFloor;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRMediumFloor SRMediumFloor}
     * 
     * @param SRMediumFloor the {@link ReservationPreferences#SRMediumFloor
     *                      SRMediumFloor}
     */
    public void setSRMediumFloor(int SRMediumFloor) {
        this.SRMediumFloor = SRMediumFloor;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRAccessibleRoom
     * SRAccessibleRoom}
     * 
     * @param SRAcessibleRoom the {@link ReservationPreferences#SRAccessibleRoom
     *                        SRAccessibleRoom}
     */
    public void setSRAcessibleRoom(int SRAcessibleRoom) {
        this.SRAccessibleRoom = SRAcessibleRoom;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRBathtub SRBathtub}
     * 
     * @param SRBathub the {@link ReservationPreferences#SRBathtub SRBathtub}
     */
    public void setSRBathtub(int SRBathtub) {
        this.SRBathtub = SRBathtub;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRShower SRShower}
     * 
     * @param SRShower the {@link ReservationPreferences#SRShower SRShower}
     */
    public void setSRShower(int SRShower) {
        this.SRShower = SRShower;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRCrib SRCrib}
     * 
     * @param SRCrib the {@link ReservationPreferences#SRCrib SRCrib}
     */
    public void setSRCrib(int SRCrib) {
        this.SRCrib = SRCrib;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRKingSizeBed SRKingSizeBed}
     * 
     * @param SRKingSizeBed the {@link ReservationPreferences#SRKingSizeBed
     *                      SRKingSizeBed}
     */
    public void setSRKingSizeBed(int SRKingSizeBed) {
        this.SRKingSizeBed = SRKingSizeBed;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRNearElevator
     * SRNearElevator}
     * 
     * @param SRNearElevator the {@link ReservationPreferences#SRNearElevator
     *                       SRNearElevator}
     */
    public void setSRNearElevator(int SRNearElevator) {
        this.SRNearElevator = SRNearElevator;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRAwayFromElevator
     * SRAwayFromElevator}
     * 
     * @param SRAwayFromElevator the
     *                           {@link ReservationPreferences#SRAwayFromElevator
     *                           SRAwayFromElevator}
     */
    public void setSRAwayFromElevator(int SRAwayFromElevator) {
        this.SRAwayFromElevator = SRAwayFromElevator;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRQuietRoom SRQuietRoom}
     * 
     * @param SRQuietRoom the {@link ReservationPreferences#SRQuietRoom SRQuietRoom}
     */
    public void setSRQuietRoom(int SRQuietRoom) {
        this.SRQuietRoom = SRQuietRoom;
    }

    /**
     * Sets the attribute {@link ReservationPreferences#SRNoAlcoholInMiniBar SRNoAlcoholInMiniBar}
     * 
     * @param SRNoAlcoholInMiniBar the {@link ReservationPreferences#SRNoAlcoholInMiniBar SRNoAlcoholInMiniBar}
     */
    public void setSRNoAlcoholInMiniBar(int SRNoAlcoholInMiniBar) {
        this.SRNoAlcoholInMiniBar = SRNoAlcoholInMiniBar;
    }

    @Override
    public String toString() {
        return "ReservationPreferences [ID=" + ID + ", SRHighFloor=" + SRHighFloor + ", SRLowFloor=" + SRLowFloor
                + ", SRMediumFloor=" + SRMediumFloor + ", SRAccessibleRoom=" + SRAccessibleRoom + ", SRBathtub="
                + SRBathtub + ", SRShower=" + SRShower + ", SRCrib=" + SRCrib + ", SRKingSizeBed=" + SRKingSizeBed
                + ", SRTwinBed=" + SRTwinBed + ", SRNearElevator=" + SRNearElevator + ", SRAwayFromElevator="
                + SRAwayFromElevator + ", SRQuietRoom=" + SRQuietRoom + ", SRNoAlcoholInMiniBar=" + SRNoAlcoholInMiniBar
                + "]";
    }
}