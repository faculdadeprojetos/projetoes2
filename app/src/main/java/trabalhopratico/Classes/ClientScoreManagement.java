package trabalhopratico.Classes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import trabalhopratico.Interfaces.IClientScoreManagement;

public class ClientScoreManagement implements IClientScoreManagement {
    /*
     * Array of clients that will contain the information of all clients
     */
    private ArrayList<Client> clientes;

    /*
     * Constructor for the array of clients
     */
    public ClientScoreManagement() {
        this.clientes = new ArrayList<>();
    }

    /**
     * Returns the array list of clients
     * 
     * @return array list of clients
     */
    public ArrayList<Client> getClients() {
        return this.clientes;
    }

    public void removeDuplicates() {
        Map<String, Client> map = new HashMap<>();

        for (Client client : this.clientes) {
            String docIDHash = client.getDocIDHash();
            if (!map.containsKey(docIDHash)) {
                // docIDHash is not a duplicate, so add the client to the map
                map.put(docIDHash, client);
            } else {
                // docIDHash is a duplicate, so add the reservation from the duplicate to the
                // first instance
                map.get(docIDHash).getReservations().add(client.getReservations().get(0));
            }
        }

        this.clientes = new ArrayList<>(map.values());
        Collections.sort(this.clientes, new Comparator<Client>() {
            @Override
            public int compare(Client c1, Client c2) {
                return c1.getId() - c2.getId();
            }
        });
    }

    /*
     * Adds the regularity score to all clients based on the frequency of the
     * purchases they've made
     */
    @Override
    public String addRegularidadeScore() {
        if (this.clientes.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        
        for (int i = 0; i < this.clientes.size(); i++) {
            int diasReservas = 0;
            ArrayList<LocalDate> datas = new ArrayList<>();

            if (this.clientes.get(i).getReservations().size() <= 1) // Caso exista apenas uma reserva
            {
                diasReservas = 0;
            } else {
                diasReservas = 0;
                datas = new ArrayList<>();

                for (int j = 0; j < this.clientes.get(i).getReservations().size(); j++) {
                    datas.add(this.clientes.get(i).getReservations().get(j).getPurchasesDate());
                }

                // Ordenar array de acordo com as datas
                Collections.sort(datas);

                // somando os intervalos entre reservas
                for (int l = 0; l < datas.size() - 1; l++) {
                    diasReservas += (datas.get(l + 1).toEpochDay()) - (datas.get(l).toEpochDay());
                }

                diasReservas /= this.getClients().get(i).getReservations().size();
                // System.out.println("DIAS RESERVAS: " + diasReservas);
            }

            if (diasReservas > 0 && diasReservas <= 78) {
                this.getClients().get(i).getEstatisticasCliente().setRegularidadeScore(4);

            } else if (diasReservas >= 79 && diasReservas <= 159) {
                this.getClients().get(i).getEstatisticasCliente().setRegularidadeScore(3);

            } else if (diasReservas >= 160 && diasReservas <= 245) {
                this.getClients().get(i).getEstatisticasCliente().setRegularidadeScore(2);

            } else {
                this.getClients().get(i).getEstatisticasCliente().setRegularidadeScore(1);
            }
        }
    
        return "Successful";
    }

    /*
     * Adds the total number of purchases score to all clients based on the amount
     * of purchases they've made
     */
    @Override
    public String addTotalComprasScore() {
        
        if (this.clientes.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        for (int i = 0; i < this.clientes.size(); i++) {
            if (clientes.get(i).getReservations().size() == 1) {
                clientes.get(i).getEstatisticasCliente().setTotalComprasScore(1);
            }
            if (clientes.get(i).getReservations().size() == 2) {
                clientes.get(i).getEstatisticasCliente().setTotalComprasScore(2);
            }
            if (clientes.get(i).getReservations().size() == 3 || clientes.get(i).getReservations().size() == 4
                    || clientes.get(i).getReservations().size() == 5) {
                clientes.get(i).getEstatisticasCliente().setTotalComprasScore(3);
            }
            if (clientes.get(i).getReservations().size() > 5) {
                clientes.get(i).getEstatisticasCliente().setTotalComprasScore(4);
            }
        }
        return "Sucessful";
    }

    /*
     * Adds the monetization score to all clients based on the money they've spent
     */
    @Override
    public String addMonetizacaoScore() {
        if (this.clientes.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        for (int i = 0; i < this.clientes.size(); i++) {
            double dinheiroGasto = 0;

            for (int j = 0; j < this.clientes.get(i).getReservations().size(); j++) {
                dinheiroGasto += this.clientes.get(i).getReservations().get(j).getLodgingRevenue()
                        + this.clientes.get(i).getReservations().get(j).getOtherRevenue();
            }

            dinheiroGasto /= this.getClients().get(i).getReservations().size();

            if (dinheiroGasto >= 0 && dinheiroGasto <= 100) {
                this.getClients().get(i).getEstatisticasCliente().setMonetizacaoScore(1);
            } else if (dinheiroGasto >= 101 && dinheiroGasto <= 250) {
                this.getClients().get(i).getEstatisticasCliente().setMonetizacaoScore(2);
            } else if (dinheiroGasto >= 251 && dinheiroGasto <= 500) {
                this.getClients().get(i).getEstatisticasCliente().setMonetizacaoScore(3);
            } else {
                this.getClients().get(i).getEstatisticasCliente().setMonetizacaoScore(4);
            }
        }

        return "Sucessful";
    }
}