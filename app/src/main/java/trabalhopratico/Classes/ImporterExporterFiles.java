package trabalhopratico.Classes;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import trabalhopratico.Enumerations.DistributionChannel;
import trabalhopratico.Enumerations.MarketSegment;
import trabalhopratico.Interfaces.IImporterExporterFiles;


public class ImporterExporterFiles implements IImporterExporterFiles {

    // Constructor : create instances of ImporterExporterFiles
    public ImporterExporterFiles() {
    }

    @Override
    public String importer(String fileName, ArrayList<Client> clients, ArrayList<Reservation> reservations)
            throws IOException, CsvException, ParseException {

        // Create a CSVReader object
        CSVReader reader = new CSVReaderBuilder(new FileReader("" + fileName))
                .withCSVParser(new CSVParserBuilder().withSeparator(';').build()).withSkipLines(1).build();

        // Read All the data from the CSVFile
        List<String[]> allData = reader.readAll();


        if (allData.isEmpty()) {
            throw new IllegalArgumentException("File can't be empty");
        }
        /**
         * Store the data from CSVFile line by line
         * Create instances of Client, Reservation and ReservationPreferences using the
         * data stored
         **/
        for (String[] data : allData) {

            try {
                int id = Integer.parseInt(data[0]);
                String docIDHash = data[5];
                double lodgingRevenue = Double.parseDouble(data[7].replace(',', '.'));
                double otherRevenue = Double.parseDouble(data[8].replace(',', '.'));
                int bookingsCanceled = Integer.parseInt(data[9]);
                int bookingsNoShowed = Integer.parseInt(data[10]);
                int bookingsCheckedIn = Integer.parseInt(data[11]);
                int personsNight = Integer.parseInt(data[12]);
                int roomNights = Integer.parseInt(data[13]);
                int daysSinceLastStay = Integer.parseInt(data[14]);
                int daysSinceFirstStay = Integer.parseInt(data[15]);
                DistributionChannel distributionChannel = DistributionChannel
                        .valueOf(data[16].replaceAll("\\s+", "_").replaceAll("/", "_").toUpperCase());
                MarketSegment marketSegment = MarketSegment
                        .valueOf(data[17].replaceAll("\\s+", "_").replaceAll("/", "_").toUpperCase());
                int srHighFloor = Integer.parseInt(data[18]);
                int srLowFloor = Integer.parseInt(data[19]);
                int srAcessibleRoom = Integer.parseInt(data[20]);
                int srMediumFloor = Integer.parseInt(data[21]);
                int srBathtub = Integer.parseInt(data[22]);
                int srShower = Integer.parseInt(data[23]);
                int srCrib = Integer.parseInt(data[24]);
                int srKingSizeBed = Integer.parseInt(data[25]);
                int srTwinBed = Integer.parseInt(data[26]);
                int srNearElevator = Integer.parseInt(data[27]);
                int srAwayFromElevator = Integer.parseInt(data[28]);
                int srNoAlcoholInMiniBar = Integer.parseInt(data[29]);
                int srQuietRoom = Integer.parseInt(data[30]);

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
                LocalDate purchaseDate = LocalDate.parse(data[31], formatter);
                
                int paymentMethod = Integer.parseInt(data[32]);

                // Create a instance of ReservationPreferences
                ReservationPreferences preferences = new ReservationPreferences(id, srHighFloor, srLowFloor,
                        srMediumFloor, srAcessibleRoom, srBathtub, srShower,
                        srCrib, srKingSizeBed, srTwinBed, srNearElevator, srAwayFromElevator, srQuietRoom,
                        srNoAlcoholInMiniBar);

                ArrayList<ReservationPreferences> reservationsPreferences = new ArrayList<>();

                //Create a instance of Reservation
                Reservation reservation = new Reservation (id, personsNight, roomNights, daysSinceFirstStay, daysSinceLastStay, lodgingRevenue, otherRevenue,
                purchaseDate, paymentMethod, distributionChannel, marketSegment);

                // Add a ReservationPreferences instance to the Reservation
                reservationsPreferences.add(preferences);

                // Add a Reservation to an ArrayList of reservations for an easier time
                // providing the statistics requested by the assignment
                reservations.add(reservation);

                // Associate a preferences to the reservation
                reservation.getPreferenceReservation().add(preferences);

                // Create a instance of Client
                Client client = new Client(id, docIDHash, bookingsCanceled, bookingsNoShowed, bookingsCheckedIn);

                // Associate a reservation to the client
                client.getReservations().add(reservation);

                //Add a client to an ArrayList of Client
                clients.add(client);

            } catch (NumberFormatException nfe) {
                return nfe.getMessage();
            }
        }

        return "Successful";
    }

    @Override
    public String exporter(String fileName, ArrayList<Client> clientes, StatisticsGeral estatisticas) throws IOException {
        if (clientes.isEmpty()) {
            throw new IllegalArgumentException("A lista de clientes está vazia");
        }

        if (fileName.trim().equals("")) {
            throw new IOException("O ficheiro em que está a tentar escrever não existe");
        }

        System.out.println(fileName);

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
        JsonObject jsonObject = new JsonObject();

        jsonObject.add("statistics", gson.toJsonTree(estatisticas));

        JsonArray clientArray = new JsonArray();
        
        for (Client client : clientes) {
            clientArray.add(gson.toJsonTree(client));
        }

        jsonObject.add("clientes", gson.toJsonTree(clientArray));

        FileWriter writer = new FileWriter(fileName);
        writer.write(gson.toJson(jsonObject));
        writer.flush();
        writer.close();

        return "Successful";
    }
}
