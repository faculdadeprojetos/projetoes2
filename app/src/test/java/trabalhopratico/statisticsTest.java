package trabalhopratico;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.opencsv.exceptions.CsvException;

import trabalhopratico.Classes.Client;
import trabalhopratico.Classes.ClientScoreManagement;
import trabalhopratico.Classes.ImporterExporterFiles;
import trabalhopratico.Classes.Reservation;
import trabalhopratico.Classes.ReservationManagement;
import trabalhopratico.Classes.StatisticsGeral;
import trabalhopratico.Enumerations.DistributionChannel;

public class statisticsTest {

    private ClientScoreManagement clients;

    private ReservationManagement reservations;

    private ImporterExporterFiles importer;

    private StatisticsGeral statistics;

    /**
     * 
     */
    public statisticsTest() {
        clients = new ClientScoreManagement();
        reservations = new ReservationManagement();
        importer = new ImporterExporterFiles();
        statistics = new StatisticsGeral();
    }

    // #region IMPORT

    @BeforeEach
    public void importBeforeTesting() {
        File file = new File("Files/Dataset_nova_versao.csv");
        String path = file.getAbsolutePath();

        try {
            importer.importer(path, clients.getClients(), reservations.getReservas());
        } catch (IOException | CsvException | ParseException e) {
            e.printStackTrace();
        }

        clients.removeDuplicates();
    }

    // #endregion

    // #region PAYMENT METHOD

    @DisplayName("Prefered payment method for an valid client in the array of clients")
    @Test
    public void testGetPaymentMethodClient_ReturnThePreferedPaymentMethodByClient_Valid() {
        String expected = "Payment Providers";

        Assertions.assertEquals(expected, statistics.getPaymentMethodByClient(clients.getClients().get(0)));
    }

    @DisplayName("Prefered payment method for an invalid client in the array of clients")
    @Test
    public void testGetPaymentMethodClient_ReturnIndexOutOfBoundsException_Valid_1() {
        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getPaymentMethodByClient(clients.getClients().get(-1)));
    }

    @DisplayName("Prefered payment method for a client without reservations")
    @Test
    public void testGetPaymentMethodClient_ReturnIndexOutOfBoundsException_Valid_2() {
        ArrayList<Client> tempClientes = new ArrayList<>();

        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getPaymentMethodByClient(tempClientes.get(0)));
    }

    @DisplayName("Prefered payment method for a null reference")
    @Test
    public void testGetPaymentMethodClient_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class, () -> statistics.getPaymentMethodByClient(null));
    }

    @DisplayName("Prefered payment method used by most clients for an array list with valid clients")
    @Test
    public void testGetPaymentMethodGeneral_ReturnThePreferedPaymentMethodGeneral_Valid() {
        String expected = "Transferencia Bancaria";

        Assertions.assertEquals(expected, statistics.getPaymentMethodGeneral(reservations.getReservas()));
    }

    @DisplayName("Prefered payment method used by most clients for an empty array list")
    @Test
    public void testGetPaymentMethodGeneral_ReturnIllegalArgumentException_Valid() {
        ArrayList<Reservation> tempReservations = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getPaymentMethodGeneral(tempReservations));
    }

    @DisplayName("Prefered payment method used by most clients for a null reference")
    @Test
    public void testGetPaymentMethodGeneral_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class, () -> statistics.getPaymentMethodGeneral(null));
    }

    // #endregion

    // #region MAX VALUE SPENT ON RESERVATION

    @DisplayName("Max value spent on a reservation comparing all clients")
    @Test
    public void testGetMaxValueReservation_ReturnTheReservationWithMaxValue_Valid() {
        double expected = 23365.0;

        Assertions.assertEquals(expected, statistics.getMaxValueReservation(reservations.getReservas()), 0);
    }

    @DisplayName("Max value spent on a reservation with an empty array list")
    @Test
    public void testGetMaxValueReservation_ReturnIllegalArgumentException_Valid() {
        ArrayList<Reservation> tempReservations = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getMaxValueReservation(tempReservations));
    }

    @DisplayName("Max value spent on a reservation with a null reference")
    @Test
    public void testGetMaxValueReservation_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class, () -> statistics.getMinValueReservation(null));
    }

    // #endregion

    // #region MIN VALUE SPENT ON RESERVATION

    @DisplayName("Min value spent on a reservation comparing all clients")
    @Test
    public void testGetMinValueReservation_ReturnTheReservationWithMinValue_Valid() {
        double expected = 0;

        Assertions.assertEquals(expected, statistics.getMinValueReservation(reservations.getReservas()), 0);
    }

    @DisplayName("Min value spent on a reservation with an empty array list")
    @Test
    public void testGetMinValueReservation_ReturnIllegalArgumentException_Valid() {
        ArrayList<Reservation> tempReservations = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getMinValueReservation(tempReservations));
    }

    @DisplayName("Min value spent on a reservation with a null reference")
    @Test
    public void testGetMinValueReservation_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class, () -> statistics.getMinValueReservation(null));
    }

    // #endregion

    // #region AVERAGE VALUE SPENT ON RESERVATION

    @DisplayName("Average value spent on a reservation comparing all clients")
    @Test
    public void testGetAverageValueReservation_ReturnTheAverageValueOfAllReservations_Valid() {
        double expected = 366.39121892571745;

        Assertions.assertEquals(expected, statistics.getAverageValueReservation(reservations.getReservas()), 0);
    }

    @DisplayName("Average value spent on a reservation with an empty list")
    @Test
    public void testGetAverageValueReservation_ReturnIllegalArgumentException_Valid() {
        ArrayList<Reservation> tempReservations = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getAverageValueReservation(tempReservations));
    }

    @DisplayName("Average value spent on a reservation with a null reference")
    @Test
    public void testGetAverageValueReservation_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getAverageValueReservation(null));
    }

    // #endregion

    // #region CLIENT MIN VALUE SPENT ON RESERVATION

    @DisplayName("Client with lowest amount spent on a reservation comparing all clients")
    @Test
    public void testGetClientMinValueReservation_ReturnClientWithMinValueReservation_Valid() {
        Client expected = clients.getClients().get(1);

        Assertions.assertEquals(expected, statistics.getClientMinValueMonetization(clients.getClients()));
    }

    @DisplayName("Client with lowest amount spent on a reservation with an empty list")
    @Test
    public void testGetClientMinValueReservation_ReturnIllegalArgumentException_Valid() {
        ArrayList<Client> tempListaClientes = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getClientMinValueMonetization(tempListaClientes));
    }

    @DisplayName("Client with lowest amount spent on a reservation with null reference")
    @Test
    public void testGetClientMinValueReservation_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getClientMinValueMonetization(null));
    }

    // #endregion

    // #region CLIENT MAX VALUE SPENT ON RESERVATION

    @DisplayName("Client with highest amount spent on a reservation comparing all clients")
    @Test
    public void testGetClientMaxValueReservation_ReturnClientWithMaxValueReservation_Valid() {
        Client expected = clients.getClients().get(2462);

        Assertions.assertEquals(expected, statistics.getClientMaxValueMonetization(clients.getClients()));
    }

    @DisplayName("Client with highest amount spent on a reservation with empty list")
    @Test
    public void testGetClientMaxValueReservation_ReturnIllegalArgumentException_Valid() {
        ArrayList<Client> tempListaClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getClientMaxValueMonetization(tempListaClients));
    }

    @DisplayName("Client with highest amount spent on a reservation with null reference")
    @Test
    public void testGetClientMaxValueReservation_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getClientMaxValueMonetization(null));
    }

    // #endregion

    // #region PREFERED SEASONALITY

    @DisplayName("Prefered seasonality for a valid client in the array of clients")
    @Test
    public void testGetSeasonalityClient_ReturnThePreferedSeasonalityByClient_Valid() {
        String expected = "Primavera";
        Assertions.assertEquals(expected, statistics.getPreferedSeasonalityByClient(clients.getClients().get(0)));
    }

    @DisplayName("Prefered seasonality for an invalid client in the array of clients")
    @Test
    public void testGetSeasonalityClient_ReturnIndexOutOfBoundsException_Valid_1() {
        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getPreferedSeasonalityByClient(clients.getClients().get(-1)));
    }

    @DisplayName("Prefered seasonality for a client without reservations")
    @Test
    public void testGetSeasonalityClient_ReturnIndexOutOfBoundsException_Valid_2() {
        ArrayList<Client> tempClientes = new ArrayList<>();
        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getPreferedSeasonalityByClient(tempClientes.get(0)));
    }

    @DisplayName("Prefered seasonality for a null reference")
    @Test
    public void testGetSeasonalityClient_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getPreferedSeasonalityByClient(null));
    }

    @DisplayName("Prefered seasonality used by most clients for an array list with valid clients")
    @Test
    public void testGetSeasonalityGeneral_ReturnThePreferedSeasonalityGeneral_Valid() {
        String expected = "Verao";
        Assertions.assertEquals(expected, statistics.getPreferedSeasonalityGeneral(reservations.getReservas()));
    }

    @DisplayName("Prefered seasonality used by most clients for an empty array list")
    @Test
    public void testGetSeasonalityGeneral_ReturnIllegalArgumentException_Valid() {
        ArrayList<Reservation> tempReservations = new ArrayList<>();
        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getPreferedSeasonalityGeneral(tempReservations));
    }

    @DisplayName("Prefered seasonality used by most clients for a null reference")
    @Test
    public void testGetSeasonalityGeneral_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getPreferedSeasonalityGeneral(null));
    }
    // #endregion

    // #region DISTRIBUTION CHANNEL

    @DisplayName("Prefered distribution channel method for a valid array of reservas")
    @Test
    public void testGetDistributionChannelGeneral_ReturnThePreferedDistributionChannel_Valid() {
        DistributionChannel expected = DistributionChannel.TRAVEL_AGENT_OPERATOR;

        Assertions.assertEquals(expected, statistics.getDistributionChannelGeneral(reservations.getReservas()));
    }

    @DisplayName("Prefered distribution channel method for an empty array of reservas")
    @Test
    public void testGetDistributionChannelGeneral_ReturnIndexOutOfBoundsException_Valid_1() {
        ArrayList<Reservation> tempReservations = new ArrayList<>();

        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getDistributionChannelGeneral(tempReservations));
    }

    @DisplayName("Prefered distribution channel method for a null reference")
    @Test
    public void testGetDistributionChannelGeneral_ReturnNullPointerException_Valid_2() {
        ArrayList<Reservation> tempReservations = null;
        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getDistributionChannelGeneral(tempReservations));
    }

    // #endregion

    // #region INTERVAL BETWEEN PURCHASES

    // #region AVERAGE GENERAL

    @DisplayName("Average interval time between purchases general method for an valid array of clients")
    @Test
    public void testGetAverageIntervalTimeBetweenPurchasesClient_ReturnAverageIntervalTimeBetweenPurchasesGeneral_Valid() {
        double expected = 0.021832755114248117;

        Assertions.assertEquals(expected,
                statistics.getAverageIntervalTimeBetweenPurchasesGeneral(reservations.getReservas()));
    }

    @DisplayName("Average interval time between purchases general method for an empty array of clients")
    @Test
    public void testGetAverageIntervalTimeBetweenPurchasesGeneral_ReturnIndexOutOfBoundsException_Valid_1() {
        ArrayList<Reservation> tempReservations = new ArrayList<>();

        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getAverageIntervalTimeBetweenPurchasesGeneral(tempReservations));
    }

    @DisplayName("Average interval time between purchases method for a null reference")
    public void testGetAverageIntervalTimeBetweenPurchasesGeneral_ReturnIllegalArgumentException_Valid_2() {

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getAverageIntervalTimeBetweenPurchasesGeneral(null));
    }

    // #endregion

    // #region AVERAGE CLIENT

    @DisplayName("Average interval time between purchases method for an valid client in array of clients")
    @Test
    public void testGetAverageIntervalTimeBetweenPurchasesClient_ReturnAverageIntervalTimeBetweenPurchases_Valid() {
        double expected = 0.0;

        Assertions.assertEquals(expected,
                statistics.getAverageIntervalTimeBetweenPurchasesByClient(clients.getClients().get(0)));
    }

    @DisplayName("Average interval time between purchases method for an empty array of clients")
    @Test
    public void testGetAverageIntervalTimeBetweenPurchasesClient_ReturnIllegalArgumentException_Valid_1() {
        ArrayList<Client> tempClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getAverageIntervalTimeBetweenPurchasesByClient(tempClients.get(1)));
    }

    @DisplayName("Average interval time between purchases method for a null reference")
    @Test
    public void testGetAverageIntervalTimeBetweenPurchasesClient_ReturnNullPointerException_Valid_2() {
        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getAverageIntervalTimeBetweenPurchasesByClient(null));
    }

    // #endregion

    // #region MAX CLIENT

    @DisplayName("Max interval time between purchases method for an valid client in array of clients")
    @Test
    public void testGetMaxIntervalTimeBetweenPurchasesClient_ReturnMaxIntervalTimeBetweenPurchases_Valid() {
        long expected = 0;

        Assertions.assertEquals(expected,
                statistics.getMaxIntervalTimeBetweenPurchasesClient(clients.getClients().get(0)));
    }

    @DisplayName("Max interval time between purchases method for an empty array of clients")
    @Test
    public void testGetMaxIntervalTimeBetweenPurchasesClient_ReturnIllegalArgumentException_Valid_1() {
        ArrayList<Client> tempClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getMaxIntervalTimeBetweenPurchasesClient(tempClients.get(1)));
    }

    @DisplayName("Max interval time between purchases method for a null reference")
    @Test
    public void testGetMaxIntervalTimeBetweenPurchasesClient_ReturnNullPointerException_Valid_2() {

        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getMaxIntervalTimeBetweenPurchasesClient(null));
    }

    // #endregion

    @DisplayName("Minimum interval time between purchases method for an valid client in array of clients")
    @Test
    public void testGetMinIntervalTimeBetweenPurchasesClient_ReturnMinIntervalTimeBetweenPurchases_Valid() {
        long expected = 10;

        Assertions.assertEquals(expected,
                statistics.getMinIntervalTimeBetweenPurchasesClient(clients.getClients().get(20)));
    }

    @DisplayName("Minimum interval time between purchases method for an empty array of clients")
    @Test
    public void testGetMinIntervalTimeBetweenPurchasesClient_ReturnIllegalArgumentException_Valid_1() {
        ArrayList<Client> tempClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getMinIntervalTimeBetweenPurchasesClient(tempClients.get(1)));
    }

    @DisplayName("Minimum interval time between purchases method for a null reference")
    @Test
    public void testGetMinIntervalTimeBetweenPurchasesClient_ReturnNullPointerException_Valid_2() {

        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getMinIntervalTimeBetweenPurchasesClient(null));
    }

    // Testes para minimum time interval between purchases general method

    @DisplayName("Minimum interval time between purchases general method for an valid array of clients")
    @Test
    public void testGetMinIntervalTimeBetweenPurchasesGeneral_ReturnMinIntervalTimeBetweenPurchasesGeneral_Valid() {
        int expected = 0;

        Assertions.assertEquals(expected, statistics.getMinIntervalTimeBetweenPurchasesGeneral(clients.getClients()));
    }

    @DisplayName("Minimum interval time between purchases general method for an empty array of clients")
    @Test
    public void testGetMinIntervalTimeBetweenPurchasesGeneral_ReturnIndexOutOfBoundsException_Valid_1() {
        ArrayList<Client> tempClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getMinIntervalTimeBetweenPurchasesGeneral(tempClients));
    }

    @DisplayName("Minimum interval time between purchases method for a null reference")
    @Test
    public void testGetMinIntervalTimeBetweenPurchasesGeneral_ReturnNullPointerException_Valid() {

        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getMinIntervalTimeBetweenPurchasesGeneral(null));
    }

    // Testes para maximum time interval between purchases to a client method

    @DisplayName("Max interval time between purchases general method for an valid array of clients")
    @Test
    public void testGetMaxIntervalTimeBetweenPurchasesGeneral_ReturnMaxIntervalTimeBetweenPurchasesGeneral_Valid() {
        int expected = 1054;

        Assertions.assertEquals(expected, statistics.getMaxIntervalTimeBetweenPurchasesGeneral(clients.getClients()));

    }

    @DisplayName("Max interval time between purchases general method for an empty array of clients")
    @Test
    public void testGetMaxIntervalTimeBetweenPurchasesGeneral_ReturnIndexOutOfBoundsException_Valid_1() {
        ArrayList<Client> tempClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IndexOutOfBoundsException.class,
                () -> statistics.getMaxIntervalTimeBetweenPurchasesGeneral(tempClients));
    }

    @DisplayName("Max interval time between purchases method for a null reference")
    @Test
    public void testGetMaxIntervalTimeBetweenPurchasesGeneral_ReturnNullPointerException_alid_2() {

        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getMaxIntervalTimeBetweenPurchasesGeneral(null));
    }

    // #endregion

    // #region ALL SCORE

    @DisplayName("Client who books most often compared to all clients")
    @Test
    public void testGetClientMaxRegularity_ReturnClientMaxRegularity_Valid() {
        Client expected = clients.getClients().get(73916);

        Assertions.assertEquals(expected, statistics.getClientMaxRegularity(clients.getClients()));
    }

    @DisplayName("Client who books most often with empty list")
    @Test
    public void testGetClientMaxRegularity_ReturnIllegalArgumentException_Valid() {
        ArrayList<Client> tempListClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getClientMaxRegularity(tempListClients));
    }

    @DisplayName("Client who books most often with null reference")
    @Test
    public void testGetClientMaxRegularity_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class, () -> statistics.getClientMaxRegularity(null));
    }

    @DisplayName("Client who books less often compared to all clients")
    @Test
    public void testGetClientMinRegularity_ReturnClientMinRegularity_Valid() {
        Client expected = clients.getClients().get(6356);

        Assertions.assertEquals(expected, statistics.getClientMinRegularity(clients.getClients()));
    }

    @DisplayName("Client who books less often with empty list")
    @Test
    public void testGetClientMinRegularity_ReturnIllegalArgumentException_Valid() {
        ArrayList<Client> tempListClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getClientMinRegularity(tempListClients));
    }

    @DisplayName("Client who books less often with null reference")
    @Test
    public void testGetClientMinRegularity_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class, () -> statistics.getClientMinRegularity(null));
    }


    @DisplayName("Client who have worst score average")
    @Test
    public void testGetClientWorstAverageScore_ReturnClientWorstAverageScore_Valid() {
        clients.addTotalComprasScore();
        clients.addRegularidadeScore();
        clients.addMonetizacaoScore();

        Client expected = clients.getClients().get(2);

        Assertions.assertEquals(expected, statistics.getClientWorstAverageScore(clients.getClients()));
    }

    @DisplayName("Client who have worst score average with empty list")
    @Test
    public void testGetClientWorstAverageScore_ReturnIllegalArgumentException_Valid() {
        ArrayList<Client> tempListClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getClientWorstAverageScore(tempListClients));
    }

    @DisplayName("Client who have worst score average with null reference")
    @Test
    public void testGetClientWorstAverageScore_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getClientWorstAverageScore(null));
    } 





    // #endregion


    //Testes getClientMaxTotalBookings

    @DisplayName("Customer with highest total purchases compared to all customers")
    @Test
    public void testGetClientMaxTotalBookings_ReturnClientMaxTotalBookings_Valid() {
        Client expected = clients.getClients().get(1);

       Assertions.assertEquals(expected, statistics.getClientMaxTotalBookings(clients.getClients()));
    }

    @DisplayName("Customer with most purchases made from an empty list")
    @Test
    public void testGetClientMaxTotalBookings_ReturnIllegalArgumentException_Valid() {
        ArrayList<Client> tempListClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getClientMaxTotalBookings(tempListClients));
    }

    @DisplayName("Customer with most purchases made from a null reference")
    @Test
    public void testGetClientMaxTotalBookings_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class, () -> statistics.getClientMaxTotalBookings(null));
    }


    // Testes getClientMinTotalBookings

    @DisplayName("Customer with lowest total purchases compared to all customers")
    @Test
    public void testGetClientMinTotalBookings_ReturnClientMinTotalBookings_Valid() {
        Client expected = clients.getClients().get(0);

        Assertions.assertEquals(expected, statistics.getClientMinTotalBookings(clients.getClients()));
    }

    @DisplayName("Customer with fewer purchases made from an empty list")
    @Test
    public void testGetClientMinTotalBookings_ReturnIllegalArgumentException_Valid() {
        ArrayList<Client> tempListClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getClientMinTotalBookings(tempListClients));
    }

    @DisplayName("Customer with fewer purchases made from a null reference")
    @Test
    public void testGetClientMinTotalBookings_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class, () -> statistics.getClientMinTotalBookings(null));
    }

    // Testes getClientBetterAverageScore 


    @DisplayName("Client who have Better score average")
    @Test
    public void testGetClientBetterAverageScore_ReturnClientBetterAverageScore_Valid() {
        clients.addTotalComprasScore();
        clients.addRegularidadeScore();
        clients.addMonetizacaoScore();

        Client expected = clients.getClients().get(1942);

        Assertions.assertEquals(expected, statistics.getClientBetterAverageScore(clients.getClients()));
    }

    @DisplayName("Client who have worst score average with empty list")
    @Test
    public void testGetClientBetterAverageScore_ReturnIllegalArgumentException_Valid() {
        ArrayList<Client> tempListClients = new ArrayList<>();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class,
                () -> statistics.getClientBetterAverageScore(tempListClients));
    }

    @DisplayName("Client who have worst score average with null reference")
    @Test
    public void testGetClientBetterAverageScore_ReturnNullPointerException_Valid() {
        Assertions.assertThrows(java.lang.NullPointerException.class,
                () -> statistics.getClientBetterAverageScore(null));
    } 
}
